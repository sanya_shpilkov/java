package com.firsthomework.secondtask;

public interface Algorithm {
    int[] calculate();
}